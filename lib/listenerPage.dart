import 'dart:async';
import 'package:flutter/material.dart';
import 'package:sycageste/homePage.dart';
class ListenerPage extends StatefulWidget {
  const ListenerPage({ Key? key }) : super(key: key);

  @override
  _ListenerPageState createState() => _ListenerPageState();
}

class _ListenerPageState extends State<ListenerPage> {
   int _downCounter = 0;
  int _upCounter = 0;
  double x = 0.0;
  double y = 0.0;

  //ELEMENT POUR LE COMPTE A REBOURE
  int _counter = 5;
  Timer? _timer;

  void _startTimer() {
    _counter = 10;
    if (_timer != null) {
      _timer?.cancel();
    }
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        if (_counter > 0) {
          _counter--;
        } else {
          _timer?.cancel();
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => const Home()));
        }
      });
    });
  }
  //FIN

  void _incrementDown(PointerEvent details) {
    _updateLocation(details);
    setState(() {
      _downCounter++;
    });
  }

  void _incrementUp(PointerEvent details) {
    _updateLocation(details);
    setState(() {
      _upCounter++;
      _startTimer();
    });
  }

  void _updateLocation(PointerEvent details) {
    setState(() {
      x = details.position.dx;
      y = details.position.dy;
    });

  }

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.tight(const Size(300.0, 200.0)),
      child: Listener(
        onPointerDown: _incrementDown,
        onPointerMove: _updateLocation,
        onPointerUp: _incrementUp,
        child: Container(
          color: Colors.lightBlueAccent,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Text('Statistique:'),
              Text(
                '$_downCounter pressés\n$_upCounter relachés',
                style: const TextStyle(fontSize: 25),
              ),
              Text(
                'le curseur est: (${x.toStringAsFixed(2)}, ${y.toStringAsFixed(2)})',
              ),
            ],
          ),
        ),
      ),
    );
  }
}