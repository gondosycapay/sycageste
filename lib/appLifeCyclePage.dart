// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:sycageste/homePage.dart';
class AppLifeCyclePage extends StatefulWidget {
  const AppLifeCyclePage({ Key? key }) : super(key: key);

  @override
  _AppLifeCyclePageState createState() => _AppLifeCyclePageState();
}

class _AppLifeCyclePageState extends State<AppLifeCyclePage> with WidgetsBindingObserver{
    @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addObserver(this);
  }
  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance?.removeObserver(this);
  }
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.inactive:
        print("Application est inactive");
         Navigator.push(context,MaterialPageRoute(builder: (context)=>const Home()));
        break;
      case AppLifecycleState.paused:
        print("L'application n'est actuellement pas visible pour l'utilisateur");
        break;
      case AppLifecycleState.resumed:
        print("Application est lancé");
        break;
      case AppLifecycleState.detached:
        print("L'application est supendu");
        break;
      default:
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Connexion"),
          centerTitle: true,
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Center(
              child: Text(
                "Connexion",
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 22),
              ),
            ),
          ],
        ),
    );
  }
}