// ignore_for_file: avoid_print
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:sycageste/homePage.dart';

class CombinaisonPage extends StatefulWidget {
  const CombinaisonPage({ Key? key }) : super(key: key);

  @override
  _CombinaisonPageState createState() => _CombinaisonPageState();
}

class _CombinaisonPageState extends State<CombinaisonPage>  with WidgetsBindingObserver{
  int _downCounter = 0;
  int _upCounter = 0;
  double x = 0.0;
  double y = 0.0;
  int _counter = 5;
  Timer? _timer;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addObserver(this);
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance?.removeObserver(this);
  }

  //FONCTION DE GESTION DU CYCLE DE VIE CONCERNANT
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.inactive:
        print("Application est inactive");
         Navigator.push(context,MaterialPageRoute(builder: (context)=>const Home()));
        break;
      case AppLifecycleState.paused:
        print("L'application n'est actuellement pas visible pour l'utilisateur");
        Navigator.push(context,MaterialPageRoute(builder: (context)=>const Home()));
        break;
      case AppLifecycleState.resumed:
        print("Application est lancé");
        break;
      case AppLifecycleState.detached:
        print("L'application est supendu");
        break;
      default:
    }
  }

  //FONCTION POUR LE COMPTE A REBOURE
  void _startTimer() {
    _counter = 10;
    if (_timer != null) {
      _timer?.cancel();
    }
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        if (_counter > 0) {
          _counter--;
        } else {
          _timer?.cancel();
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => const Home()));
        }
      });
    });
  }

  //FONCTION DE GESTION DU POINTEUR DETERMINANT SI L'ECRAN EST TOUCHER
  void _incrementDown(PointerEvent details) {
    _updateLocation(details);
    setState(() {
      _downCounter++;
    });
  }

  //FONCTION DE GESTION DU POINTEUR DETERMINANT SI L'ECRAN N'EST PAS TOUCHER
  void _incrementUp(PointerEvent details) {
    _updateLocation(details);
    setState(() {
      _upCounter++;
      _startTimer();
    });
  }

  //FONCTION DE GESTION DU POINTEUR DETERMINANT LA POSITION DU CURSEUR
  void _updateLocation(PointerEvent details) {
    setState(() {
      x = details.position.dx;
      y = details.position.dy;
    });

  }

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.tight(const Size(300.0, 200.0)),
      child: Listener(
        onPointerDown: _incrementDown,
        onPointerMove: _updateLocation,
        onPointerUp: _incrementUp,
        child: Container(
          color: Colors.lightBlueAccent,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Text('Statistique:'),
              Text(
                '$_downCounter pressés\n$_upCounter relachés',
                style: const TextStyle(fontSize: 25),
              ),
              Text(
                'le curseur est: (${x.toStringAsFixed(2)}, ${y.toStringAsFixed(2)})',
              ),
            ],
          ),
        ),
      ),
    );
  }
}